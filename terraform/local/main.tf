terraform {   
  backend "http" { 
    update_method = "PUT" # OCI
    # address / TF_HTTP_ADDRESS
    #address = "${var.HTTP_ADDRESS}basic_auto.tfstate"
  }

  required_providers {     
    checkpoint = {        
      version = ">=1.4.0"
      source = "CheckPointSW/checkpoint"     
    }
  }
}

variable "CHECKPOINT_DOMAIN" {} # export TF_VAR_CHECKPOINT_DOMAIN
provider "checkpoint" {
  # server = var.CHECKPOINT_SERVER
  # username = var.CHECKPOINT_USERNAME
  # password = var.CHECKPOINT_PASSWORD
  # context = "web_api"
  # server = "172.23.23.19"
  domain = var.CHECKPOINT_DOMAIN
}


resource "checkpoint_management_host" "test1" {
  name = "test1"
  ipv4_address = "10.10.10.10"
  color = "red"
}

resource "checkpoint_management_host" "test2" {
  name = "test2"
  ipv4_address = "10.10.20.20"
  color = "cyan"
  nat_settings = {
    auto_rule = true
    method = "hide"
    hide_behind  = "gateway" # "gateway"  "ip-address"
    # ipv4_address = "1.1.1.1"
    install_on = "All"
  } 
}


resource "checkpoint_management_access_rule" "rule1" {
  layer = "Network"
  position = {top = "top"} # {below = checkpoint_management_access_rule.rule1.name}
  name = "ruletest1"
  action = "Accept"
  enabled = true
  source = [checkpoint_management_host.test1.id]
  destination = ["Any"]
  service = ["Any"]
  # destination_negate = true
  track = {
    type = "Log"
    accounting = true
    alert = "none"
    enable_firewall_session = false
    per_connection = true
    per_session = false
  }
  # depends_on  = [checkpoint_management_host.test1]
}


# -= Final publishing =-
resource "checkpoint_management_publish" "publish_changes" {
  triggers = ["${timestamp()}"]
  depends_on = [checkpoint_management_access_rule.rule1]
}

