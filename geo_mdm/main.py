# pylint: disable=C0116,C0301,C0410,C0103,W1514,W1203
import sys, os, logging
# cpapi is a library that handles the communication with the Check Point management server.
# Enable MultiDomain | Blades | Management API advanced settings | All IP addresses that can be used for GUI clients
from cpapi import APIClient, APIClientArgs
import yaml
from time import sleep
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))



def login_mdm(mds_ip, mds_username, mds_password, dmn=""):
    logging.info(f"Logging to: {mds_ip}/{dmn} as {mds_username}")

    client_args = APIClientArgs(server=mds_ip)
    client = APIClient(client_args)

    # The API client, would look for the server's certificate SHA1 fingerprint in a file.
    # If the fingerprint is not found on the file, it will ask the user if he accepts the server's fingerprint.
    # In case the user does not accept the fingerprint, exit the program.
    if client.check_fingerprint() is False:
        logging.critical("Could not get the server's fingerprint - Check connectivity with the server.")
        sys.exit(1)

    response = client.login(mds_username, mds_password, domain = dmn)
    # print(login_res)

    if response.success is False:
        # print(f"\n{login_res}\n")
        response.critical(f"Login failed:\n{response.error_message}\nError: {response.data['errors'][0]['message']}\n")
        sys.exit(1)
    else:
        logging.info(f"Logged to: {mds_ip}/{dmn} as {mds_username}")

        logging.debug(f"login_mdm:\n{response}\n")
    return client # login_mdm

def create_dmn(client, mds_name, mds_dmn, mds_dmn_ip):
    response = client.api_call("show-domain", {"name": mds_dmn})
    logging.debug(f"create_dmn: show-domain\n{response}\n")
    if response.success is False:
        # Domain not found, to be created
        logging.info(f"Creating domain {mds_dmn} {mds_dmn_ip}")
        response = client.api_call("add-domain", {
                "name": mds_dmn,
                "servers": {"name": f"{mds_dmn}_Server", "ip-address": mds_dmn_ip, "multi-domain-server": mds_name},
                # "color": "blue",
                # "details-level": "full",
                }
            )
        logging.debug(f"create_dmn: add-domain\n{response}\n")

        response = client.api_call("show-domain", {"name": mds_dmn})
        logging.debug(f"create_dmn: show-domain\n{response}\n")
        if response.success:
            logging.info(f"Domain {mds_dmn} created.")
        else:
            logging.critical(f"create_dmn: Error when creating domain:\n{response}\n")
    else:
        logging.warning(f"Domain {mds_dmn} is already present")
    return response # create_dmn

def reassign_dmn(client, dependent_domain, global_domain = "global"):
    # a delay might be required after Domain Server creation to FWM starts
    RETRY = 5
    for retry in range(0,RETRY):
        response = client.api_call("assign-global-assignment", {
                "global-domains": global_domain,
                "dependent-domains": dependent_domain,
                }
            )
        if response.success:
            break
        logging.info(retry+1)
        sleep(10)

    logging.debug(f"reassign_dmn: assign-global-asssignment\n{response}\n")
    if response.success and (response.status_code == 200):
        logging.info("... [re]assigned.")
    else:
        logging.error(f"Error when reassigning global assignment: ""{response.data['tasks'][0]['comments']}""\n")
    return response # reassign_dmn

def assign_dmn(client, dependent_domain, global_domain = "global", global_access_policy = "standard", manage_protection_actions = True):
    response = client.api_call("show-global-assignment", {"global-domain": global_domain, "dependent-domain": dependent_domain})
    logging.debug(f"assign_dmn: show-global-asssignment\n{response}\n")
    if response.success:
        logging.warning(f"Assignment exists: {global_access_policy} of {global_domain} to {dependent_domain}")
        response = reassign_dmn(client, dependent_domain)
    else:
        # Assignment not found, to be created
        logging.info(f"Assigning {global_access_policy} of {global_domain} to {dependent_domain}")
        response = client.api_call("add-global-assignment", {
                "global-domain": global_domain,
                "dependent-domain": dependent_domain,
                "global-access-policy": global_access_policy,
                # "global-threat-prevention-policy" : "standard",
                "manage-protection-actions": manage_protection_actions,
                }
            )
        logging.debug(f"assign_dmn: add-global-asssignment\n{response}\n")
        if not response.success:
            logging.error(f"assign_dmn: add-global-assignment (NOT added)\n{response}\n")
        response = client.api_call("publish", {})
        logging.debug(f"assign_dmn: publish\n{response}\n")
        response = reassign_dmn(client, dependent_domain)
        if response.success and (response.status_code == 200):
            logging.info("Global assignment created.")
        else:
            logging.error(f"Error when adding global assignment: ""{response.data['tasks'][0]['comments']}""\n")
    return response # assign_dmn

def create_network(client, net_name, subnet, subnet_mask, color = "crete blue"):
    response = client.api_call("show-network", {"name": net_name})
    logging.debug(f"create_network: show-network\n{response}\n")
    if response.success is False:
        response = client.api_call("add-network", {"name": net_name, "subnet": subnet, "subnet-mask": subnet_mask, "color": color})
        logging.debug(f"create_network: add-network\n{response}\n")
        if response.success is False:
            logging.error(f"Error creating network {net_name}")
            logging.error(f"\n{response}\n")
        else:
            logging.info(f"Network {net_name} created")
    else:
        # print(response)
        logging.warning(f"Network {net_name} exists")
        response = client.api_call("set-network", {"name": net_name, "subnet": subnet, "subnet-mask": subnet_mask, "color": color})
        logging.debug(f"create_network: set-network\n{response}\n")
    return response.success # create_network

def create_host(client, host_name, host_ip, color = "crete blue"):
    response = client.api_call("show-host", {"name": host_name})
    logging.debug(f"create_host: show-host\n{response}\n")
    if response.success is False:
        response = client.api_call("add-host", {"name": host_name, "ip-address": host_ip, "color": color})
        logging.debug(f"create_host: add-host\n{response}\n")
        if response.success is False:
            logging.error(f"Error creating host {host_name}")
            logging.error(f"\n{response}\n")
        else:
            logging.info(f"Host {host_name} created")
    else:
        logging.warning(f"Host {host_name} exists")
        response = client.api_call("set-host", {"name": host_name, "ip-address": host_ip, "color": color})
        logging.debug(f"create_host: set-host\n{response}\n")
    return response.success # create_host

def get_layer(client, layer_name):
    logging.info(f"Get layer {layer_name} UID")
    response = client.api_call("show-access-layers").data['access-layers']
    logging.debug(f"get_layer: show-access-layer\n{response}\n")
    for layer in response:
        if layer['domain']['domain-type'] == "domain":
            return layer['uid']
        logging.debug(f"Layer {layer['uid']} {layer['domain']['domain-type']}\n")
    return None # get_layer

def create_section(client, layer_UID, section_name, position):
    logging.info(f"Create section {section_name} at {position}")
    response = client.api_call("show-access-section", {"layer": layer_UID, "name": section_name})
    logging.debug(f"create_section: show-access-section\n{response}\n")
    if not response.success:
        response = client.api_call("add-access-section", {"layer": layer_UID, "name": section_name, "position": position,
            "ignore-warnings": True, "details-level": "full"})
        logging.debug(f"create_section: add-access-section\n{response}\n")
    return response # create_section

def create_rule(client, layer_UID, name, position, src, dst, svc, action, track, enabled = True ):
    logging.info(f"Create rule {name} at {position}")
    response = client.api_call("show-access-rule", {"layer": layer_UID, "name": name})
    logging.debug(f"create_rule: show-access-rule\n{response}\n")
    if response.status_code == 404:
        response = client.api_call("add-access-rule", {
                "layer": layer_UID, "name": name,
                "source": src, "destination": dst, "service": svc,
                "action": action, "track": track, "position": position, "enabled": enabled,
                "ignore-warnings": True, "details-level": "full"}
            )
        logging.debug(f"create_rule: add-access-rule\n{response}\n")
    else:
        logging.warning(f"The rule: '{name}' already exists.")
        response = client.api_call("set-access-rule", {
                "layer": layer_UID, "name": name,
                "source": src, "destination": dst, "service": svc,
                "action": action, "track": track, "position": position, "enabled": enabled,
                "ignore-warnings": True, "details-level": "full"}
            )
        logging.debug(f"create_rule: set-access-rule\n{response}\n")

    if not response.success:
        logging.error(f"The rule: '{name}' has NOT been added/updated")
    return response # create_rule

def cleanup():
    with open("params.yml", "r", encoding='utf-8') as stream:
        try:
            params = yaml.safe_load(stream)
            logging.debug(params)
        except yaml.YAMLError as exc:
            logging.critical(exc)
            sys.exit(1)

    client = login_mdm(
        params["MDS1_IP"],
        os.getenv(params["MDS1_ENV_USERNAME"]),
        os.getenv(params["MDS1_ENV_PASSWORD"]),
        )
    response = client.api_call("delete-domain", {"name": params["MDS1_DMN"]})
    res = client.api_call("logout", {})

    client = login_mdm(
        params["MDS1_IP"],
        os.getenv(params["MDS1_ENV_USERNAME"]),
        os.getenv(params["MDS1_ENV_PASSWORD"]),
        dmn = "Global", # GLOBAL domain
        )
    response = client.api_call("delete-access-rule", {"layer": "Network", "rule-number": 1})
    response = client.api_call("delete-network", {"name": params["srcNet"]["name"]})
    response = client.api_call("delete-host", {"name": params["srcHost"]["name"]})
    response = client.api_call("delete-host", {"name": params["dstHost"]["name"]})
    # print(response)
    res = client.api_call("publish", {})
    res = client.api_call("logout", {})

    client = login_mdm(
        params["MDS2_IP"],
        os.getenv(params["MDS2_ENV_USERNAME"]),
        os.getenv(params["MDS2_ENV_PASSWORD"]),
        )
    response = client.api_call("delete-domain", {"name": params["MDS2_DMN"]})
    res = client.api_call("logout", {})

    client = login_mdm(
        params["MDS2_IP"],
        os.getenv(params["MDS1_ENV_USERNAME"]),
        os.getenv(params["MDS1_ENV_PASSWORD"]),
        dmn = "Global", # GLOBAL domain
        )
    response = client.api_call("delete-access-rule", {"layer": "Network", "rule-number": 1})
    response = client.api_call("delete-network", {"name": params["srcNet"]["name"]})
    response = client.api_call("delete-host", {"name": params["srcHost"]["name"]})
    response = client.api_call("delete-host", {"name": params["dstHost"]["name"]})
    res = client.api_call("publish", {})
    res = client.api_call("logout", {})

    sys.exit(0)
    return None # cleanup

def main():
    logging.basicConfig(format='%(levelname)s: %(message)s', stream=sys.stdout, level=logging.DEBUG) # DEBUG INFO WARNING ERROR CRITICAL
    # logging.info('We processed %d records', len(processed_records))

    if (len(sys.argv) > 1) and (sys.argv[1] == "cleanup"):
        cleanup() # comment out

    with open("params.yml", "r", encoding='utf-8') as stream:
        try:
            params = yaml.safe_load(stream)
            logging.debug(f"params.yml:\n{params}\n")
        except yaml.YAMLError as exc:
            logging.critical(exc)
            sys.exit(1)


    ## MDS Europe
    mdm_client_mds = login_mdm(
        params["MDS1_IP"],
        os.getenv(params["MDS1_ENV_USERNAME"]),
        os.getenv(params["MDS1_ENV_PASSWORD"]),
        )

    create_dmn(
        mdm_client_mds,
        params["MDS1_NAME"],
        params["MDS1_DMN"],
        params["MDS1_DMN_IP"],
        )

    # create objects in Europe GLOBAL domain
    mdm_client_dmn = login_mdm(
        params["MDS1_IP"],
        os.getenv(params["MDS1_ENV_USERNAME"]),
        os.getenv(params["MDS1_ENV_PASSWORD"]),
        dmn = "Global", # GLOBAL domain
        )

    create_host(
        mdm_client_dmn,
        host_name = params["dstHost"]["name"],
        host_ip = params["dstHost"]["ip"],
        color = "orchid",
    )

    create_network(
        mdm_client_dmn,
        net_name = params["srcNet"]["name"],
        subnet = params["srcNet"]["subnet"],
        subnet_mask = params["srcNet"]["subnet-mask"],
        color = "orchid",
    )

    res = create_rule(mdm_client_dmn,
                "Network",
                name = params["rule_name"],
                position = "top",
                src = [params["srcNet"]["name"]],
                dst = [params["dstHost"]["name"]],
                svc = [params["service"]],
                action = "Accept",
                track = "Log",
                enabled = False,
        )

    res = mdm_client_dmn.api_call("publish", {})
    res = mdm_client_dmn.api_call("logout", {})

    # assign global policy
    res = assign_dmn(mdm_client_mds, params["MDS1_DMN"])
    res = mdm_client_mds.api_call("publish", {})
    res = mdm_client_mds.api_call("logout", {})

    # create hosts/rules in local domain
    mdm_client_dmn = login_mdm(
        params["MDS1_IP"],
        os.getenv(params["MDS1_ENV_USERNAME"]),
        os.getenv(params["MDS1_ENV_PASSWORD"]),
        dmn = params["MDS1_DMN"],
        )

    create_host(
        mdm_client_dmn,
        host_name = params["srcHost"]["name"],
        host_ip = params["srcHost"]["ip"],
        color = "dark green",
    )

    layer_UID = get_layer(mdm_client_dmn, "Network") # There are two "Network" layers - from Local and Global domains, need UUID
    # print(layer_UID)
    res = create_section(mdm_client_dmn, layer_UID, "Final", 1)
    res = create_section(mdm_client_dmn, layer_UID, params['section'], "top")

    res = create_rule(mdm_client_dmn,
                layer_UID,
                name = params["rule_name"],
                position = {"bottom": params['section']},
                src = [params["srcHost"]["name"]],
                dst = [params["dstHost"]["name"]],
                svc = [params["service"]],
                action = "Accept",
                track = "Log",
        )

    res = mdm_client_dmn.api_call("publish", {})
    res = mdm_client_dmn.api_call("logout", {})

    ## MDS Europe end


    ## MDS USA

    mdm_client_mds = login_mdm(
        params["MDS2_IP"],
        os.getenv(params["MDS2_ENV_USERNAME"]),
        os.getenv(params["MDS2_ENV_PASSWORD"]),
        )

    create_dmn(
        mdm_client_mds,
        params["MDS2_NAME"],
        params["MDS2_DMN"],
        params["MDS2_DMN_IP"],
        )

    # create objects in USA GLOBAL domain

    mdm_client_dmn = login_mdm(
        params["MDS2_IP"],
        os.getenv(params["MDS2_ENV_USERNAME"]),
        os.getenv(params["MDS2_ENV_PASSWORD"]),
        dmn = "Global", # GLOBAL domain
        )
	
    exit(1)

    create_host(
        mdm_client_dmn,
        host_name = params["dstHost"]["name"],
        host_ip = params["dstHost"]["ip"],
        color = "violet red",
    )

    res = mdm_client_dmn.api_call("publish", {})
    res = mdm_client_dmn.api_call("logout", {})

    # assign global policy
    res = assign_dmn(mdm_client_mds, params["MDS2_DMN"])
    res = mdm_client_mds.api_call("publish", {})
    res = mdm_client_mds.api_call("logout", {})

    # create hosts/rules in local domain
    mdm_client_dmn = login_mdm(
        params["MDS2_IP"],
        os.getenv(params["MDS2_ENV_USERNAME"]),
        os.getenv(params["MDS2_ENV_PASSWORD"]),
        dmn = params["MDS2_DMN"],
        )

    create_host(
        mdm_client_dmn,
        host_name = params["srcHost"]["name"],
        host_ip = params["srcHost"]["ip"],
        color = "blue",
    )

    layer_UID = get_layer(mdm_client_dmn, "Network") # There are two "Network" layers - from Local and Global domains, need UUID
    res = create_section(mdm_client_dmn, layer_UID, "Final", 1)
    res = create_section(mdm_client_dmn, layer_UID, params['section'], "top")

    res = create_rule(mdm_client_dmn,
                layer_UID,
                name = params["rule_name"],
                position = {"bottom": params['section']},
                src = [params["srcHost"]["name"]],
                dst = [params["dstHost"]["name"]],
                svc = [params["service"]],
                action = "Accept",
                track = "Log",
        )

    res = mdm_client_dmn.api_call("publish", {})
    res = mdm_client_dmn.api_call("logout", {})

    ## MDS USA end

    return res # main


if __name__ == "__main__":
    main()
