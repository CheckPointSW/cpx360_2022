

### domain_responce = client.api_call("show-domain", {"name": mds_dmn})

    APIResponse({
        "data": {
            "code": "generic_err_object_not_found",
            "message": "Requested object [DMN_E] not found"
        },
        "error_message": "code: generic_err_object_not_found\nmessage: Requested object [DMN_E] not found\n",
        "res_obj": {
            "data": {
                "code": "generic_err_object_not_found",
                "message": "Requested object [DMN_E] not found"
            },
            "status_code": 404
        },
        "status_code": 404,
        "success": false
    })
    APIResponse({
        "data": {
            "color": "black",
            "comments": "",
            "domain": {
                "domain-type": "mds",
                "name": "System Data",
                "uid": "a0eebc99-afed-4ef8-bb6d-fedfedfedfed"
            },
            "domain-type": "domain",
            "global-domain-assignments": [],
            "icon": "Objects/domain",
            "meta-info": {
                "creation-time": {
                    "iso-8601": "2021-10-04T20:10+0300",
                    "posix": 1633367447379
                },
                "creator": "WEB_API",
                "last-modifier": "System",
                "last-modify-time": {
                    "iso-8601": "2021-10-04T20:11+0300",
                    "posix": 1633367513717
                },
                "lock": "unlocked",
                "validation-state": "ok"
            },
            "name": "AR_POC_Domain",
            "read-only": false,
            "servers": [
                {
                    "active": true,
                    "ipv4-address": "172.23.23.164",
                    "multi-domain-server": "MDM_5150_B",
                    "name": "AR_POC_Domain_Server",
                    "type": "management server"
                }
            ],
            "tags": [],
            "type": "domain",
            "uid": "1378e1fe-75dd-4a9f-b2d2-b6b583154420"
        },
        "res_obj": {
            "data": {
                "color": "black",
                "comments": "",
                "domain": {
                    "domain-type": "mds",
                    "name": "System Data",
                    "uid": "a0eebc99-afed-4ef8-bb6d-fedfedfedfed"
                },
                "domain-type": "domain",
                "global-domain-assignments": [],
                "icon": "Objects/domain",
                "meta-info": {
                    "creation-time": {
                        "iso-8601": "2021-10-04T20:10+0300",
                        "posix": 1633367447379
                    },
                    "creator": "WEB_API",
                    "last-modifier": "System",
                    "last-modify-time": {
                        "iso-8601": "2021-10-04T20:11+0300",
                        "posix": 1633367513717
                    },
                    "lock": "unlocked",
                    "validation-state": "ok"
                },
                "name": "AR_POC_Domain",
                "read-only": false,
                "servers": [
                    {
                        "active": true,
                        "ipv4-address": "172.23.23.164",
                        "multi-domain-server": "MDM_5150_B",
                        "name": "AR_POC_Domain_Server",
                        "type": "management server"
                    }
                ],
                "tags": [],
                "type": "domain",
                "uid": "1378e1fe-75dd-4a9f-b2d2-b6b583154420"
            },
            "status_code": 200
        },
        "status_code": 200,
        "success": true
    })

