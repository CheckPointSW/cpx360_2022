# GEO distributed Multi-Domain Management as a Code 

## Idea

Separate Multi-Domain Servers (Europe and USA in this example)  
Policy changes may be duplicated to affected MDS independently  
Global domains are used for widely used objects  

## Files

### params.yml

Specifies parameters.

Credentials like `MDS1_ENV_PASSWORD: CHECKPOINT_PASSWORD_API` mean "use value of the environment variable `CHECKPOINT_PASSWORD_API` as a password"

### ./log/

`run1.log` - First run (no DMNs, empty Global domains)  
`run2.log` - Second run (idempotency)  

## Command line

`./main.py` creates the environment  
`./main.py cleanup` cleans the environment (delete Europe and USA local domains, delete objects and rules from Global domains)  
