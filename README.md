# basic_automation

## Approach

### Ansible vault

On the workstation (during development):

    export ANSIBLE_VAULT_PASSWORD_FILE=~/.vaultpw
    echo "PASSWORD" > $ANSIBLE_VAULT_PASSWORD_FILE; chmod 0600 $ANSIBLE_VAULT_PASSWORD_FILE

`ansible-vault encrypt_string PASSWORD --name Cisco_APIC_password`

In the pipeline:
- Define `ANSIBLE_VAULT_PASSWORD_FILE=~/.vaultpw` for the runner
- Prepare this file or add `echo "PASSWORD" > $ANSIBLE_VAULT_PASSWORD_FILE` to the pipeline

### CI/CD Variables 

For secrets stored in Ansible Vault:

- ANSIBLE_VAULT_PASSWORD_FILE

params.yml:

    Cisco_APIC_password: !vault |
            $ANSIBLE_VAULT;1.1;AES256
            34303338653030653863623366333566386161636538653737373435376237326236386339613331
            6463396664333761313632626164313531323338313333610a323066363539613531363837323132
            36373530623334306431383462383833323062386365666562326138333262356231343730343835
            3135323739333763650a393739316662663863643164636233316238396439303632333862666365
            3435

For secrets passed as environment variables:

- ssh_user
- ssh_pass
- mds_user
- mds_password
- api_key

Usage in `hosts`:

    [all:vars]
    ansible_ssh_user="{{ lookup('env','ssh_user') }}"


### Pipeline triggers

To run a pipeline:

`curl -X POST -F token=$env:GITLAB_POCLAB -F ref=master -F variables[TestVar]=VarTest -F variables[ACTION]="00-TestAPI" -F variables[EXTRAOPTS]="-v"  https://gitlab.com/api/v4/projects/30014199/trigger/pipeline`

Or from a scipt like

    $REF_NAME="master"
    Write-Host ""
    Write-Host "Trigger pipeline"
    Write-Host "------------------------------"
    $cfglist = @( "0-Test", "00-TestAPI", "01-dmn_vs_aci", "99-teardown")
    for( $i=0; $i -lt $cfglist.count; $i++)
    { Write-Host("$i " + $cfglist[$i]) }
    Write-Host ""
    $Select = Read-Host
    $ACTION=$cfglist[$Select]
    Write-Host "${Select} ${ACTION}"
    curl -X POST -F token=$env:GITLAB_POCLAB -F ref=$REF_NAME -F variables[TestVar]=VarTest -F variables[ACTION]=${ACTION} https://gitlab.com/api/v4/projects/30014199/trigger/pipeline

This triggers ansible playbook with the name passed as the `ACTION` variable.  
Nothing runs on pure commit (when `ACTION` is empty)

    stages:
    - exec

    job:
    stage: exec
    script:
        - ansible-playbook -i ansible/hosts ansible/${ACTION}.yml
    rules:
        - if: $ACTION


Steps maybe chained by a playbook like

    ---
    - import_playbook: A.yml
    - import_playbook: B.yml

## Ansible playbooks

### 00-TestApi.yaml

A few tests like `cp_mgmt_access_rule_facts` and pure API (get web services token via `https://{{ansible_host}}/web_api/v1.7/login`)

### 01-dmn_vs.yml

- Check if DMN {{new_domain}} exists
- Create {{new_domain}} with {{new_domain_ip}} if needed
- Create VS {{vs_name}} within domain
- Add trusted client (GUI client to new domain)

### 02-configure_policies.yml

- Create APIC objects in new domain
- Create DC Object for APIC EPG 1
- Create DC Object for APIC EPG 2
- Retrieve name for EPG1, EPG2 objects for future use in policy
- Create localhost object for IDA whitelist
- Configure policy rules with EPG1, EPG2 objects
- Configure Identity Awareness
- Configure IDA via REST API
- Create and remove dummy route to push config to VS via CLI
- Install policy


### 04-classic_host_rule.yml

- Create a basic host
- Create a rule using the new host

## Terraform

### terraform\nat\

Create hosts and a rule. One of the hosts is with NAT, so an automatic NAT rule is creted.  
If IP of the object was changed it will be revereted back on the next run (updated in place).  

## geo_mdm

Python-only: GEO distributed Multi-Domain Management as a Code  
